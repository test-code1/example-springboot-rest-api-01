# example-springboot-rest-api

## Para ejecutar unit test

./gradlew test

## Para levantar el servicio

./gradlew clean && ./gradlew bootRun

## Para probar el servicio

##### importar archivo postman/Test.postman_collection.json o cargar la siguiente información

```
url: 
http://localhost:8080/api/registry

header:
Content-Type application/json

body:

{
    "name": "Juan Rodriguez",
    "email": "juan@rodriguez.org",
    "password": "Rrerer14",
    "phones": [
        {
            "number": "1234567",
            "citycode": "1",
            "contrycode": "57"
        }
    ]
}
```