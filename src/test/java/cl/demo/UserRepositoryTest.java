package cl.demo;

import cl.demo.jpa.domain.User;
import cl.demo.jpa.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void findAllUsers() {

        Date currentDate = new Date();

        String token = "token";

        User user = new User();
        user.setActive(true);
        user.setCreatedAt(currentDate);
        user.setUpdatedAt(currentDate);
        user.setLastLogin(currentDate);
        user.setEmail("test@test.cl");
        user.setToken(token);
        user.setUuid(UUID.randomUUID());

        userRepository.save(user);

        Iterable<User> users = userRepository.findAll();

        assertThat(users).hasSize(1);
    }
}
