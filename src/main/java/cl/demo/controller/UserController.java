package cl.demo.controller;

import cl.demo.data.dto.RegistryRequestDTO;
import cl.demo.data.dto.RegistryResponseDTO;
import cl.demo.jpa.domain.User;
import cl.demo.jpa.repository.UserRepository;
import cl.demo.util.ApiException;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACSigner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.security.SecureRandom;
import java.util.Date;
import java.util.UUID;

@Controller
@RequestMapping("/api")
public class UserController {

    private Logger log = LogManager.getLogger(UserController.class);

    @Autowired
    private UserRepository userRepository;

    @ResponseBody
    @PostMapping("registry")
    public RegistryResponseDTO registry(@Valid @RequestBody RegistryRequestDTO request) throws ApiException, JOSEException {

        log.info("init registry");
        RegistryResponseDTO response = new RegistryResponseDTO();

        User user = userRepository.findFirstByEmail(request.getEmail());

        if (user != null) {
            log.error("El correo ya registrado.");
            throw new ApiException("El correo ya registrado.");
        }

        UUID uuid = UUID.randomUUID();

        Date currentDate = new Date();

        String token = getJWTToken(request.getEmail());

        user = new User();
        user.setActive(true);
        user.setCreatedAt(currentDate);
        user.setUpdatedAt(currentDate);
        user.setLastLogin(currentDate);
        user.setEmail(request.getEmail());
        user.setToken(token);
        user.setUuid(uuid);

        userRepository.save(user);

        response.setIsactive(true);
        response.setCreated(currentDate);
        response.setModified(currentDate);
        response.setLast_login(currentDate);
        response.setToken(token);
        response.setUuid(uuid);

        log.info("end registry");
        return response;
    }

    private String getJWTToken(String email) throws JOSEException {

        JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.HS256), new Payload(email));

        byte[] sharedKey = new byte[32];

        new SecureRandom().nextBytes(sharedKey);

        jwsObject.sign(new MACSigner(sharedKey));

        log.info("token: {}", jwsObject.serialize());

        return jwsObject.serialize();
    }
}
