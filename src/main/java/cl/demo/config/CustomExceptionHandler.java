package cl.demo.config;

import cl.demo.data.dto.GenericResponseDTO;
import cl.demo.util.ApiException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.nimbusds.jose.JOSEException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        GenericResponseDTO genericResponseDTO = new GenericResponseDTO();

        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            System.out.println("1: " + error.getField() + ": " + error.getDefaultMessage());

            if (error.getDefaultMessage().contains("{validatedField}")) {
                genericResponseDTO.setMensaje(error.getDefaultMessage().replace("{validatedField}", error.getField()));
                break;
            }
        }

        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            System.out.println(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        System.out.println(ex.getLocalizedMessage());

        return handleExceptionInternal(ex, genericResponseDTO, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(ApiException.class)
    protected ResponseEntity<Object> handleApiException(ApiException ex) {
        GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
        genericResponseDTO.setMensaje(ex.getMessage());

        return new ResponseEntity<>(genericResponseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(JOSEException.class)
    protected ResponseEntity<Object> handleJOSEException(JOSEException ex) {
        GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
        genericResponseDTO.setMensaje("invalid jwt");

        return new ResponseEntity<>(genericResponseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(JsonMappingException.class)
    protected ResponseEntity<Object> handleJsonMappingException(JsonMappingException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
        genericResponseDTO.setMensaje("Malformed JSON request");

        return handleExceptionInternal(ex, genericResponseDTO, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        GenericResponseDTO genericResponseDTO = new GenericResponseDTO();
        genericResponseDTO.setMensaje("Malformed JSON request");
        return handleExceptionInternal(ex, genericResponseDTO, headers, HttpStatus.BAD_REQUEST, request);
    }
}
