package cl.demo.data.dto;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

public class RegistryRequestDTO {

    @NotEmpty(message = "{validation.field.notEmpty}")
    @Size(min = 1, max = 255, message = "{validation.field.size}")
    private String name;

    @NotEmpty(message = "{validation.field.notEmpty}")
    @Size(min = 1, max = 255, message = "{validation.field.size}")
    @Email(message = "{validation.field.email}")
    private String email;

    @NotEmpty(message = "{validation.field.notEmpty}")
    @Size(min = 1, max = 255, message = "{validation.field.size}")
    @Pattern(regexp = "^[A-Z][a-z]*[0-9][0-9]$", message = "{validation.field.regexp.password}")
    private String password;

    @NotEmpty(message = "{validation.field.notEmptyList}")
    @Valid
    private List<Phones> phones;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Phones> getPhones() {
        return phones;
    }

    public void setPhones(List<Phones> phones) {
        this.phones = phones;
    }

    static class Phones {

        @NotEmpty(message = "{validation.field.notEmpty}")
        @Size(min = 1, max = 255, message = "{validation.field.size}")
        private String number;

        @NotEmpty(message = "{validation.field.notEmpty}")
        @Size(min = 1, max = 255, message = "{validation.field.size}")
        private String citycode;

        @NotEmpty(message = "{validation.field.notEmpty}")
        @Size(min = 1, max = 255, message = "{validation.field.size}")
        private String contrycode;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getCitycode() {
            return citycode;
        }

        public void setCitycode(String citycode) {
            this.citycode = citycode;
        }

        public String getContrycode() {
            return contrycode;
        }

        public void setContrycode(String contrycode) {
            this.contrycode = contrycode;
        }
    }
}
