package cl.demo.data.dto;

import java.util.Date;
import java.util.UUID;

public class RegistryResponseDTO {

    private UUID uuid;
    private Date created;
    private Date modified;
    private Date last_login; //(en caso de nuevo usuario, va a coincidir con la fecha de creación)
    private String token; // (puede ser UUID o JWT)
    private boolean isactive;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getLast_login() {
        return last_login;
    }

    public void setLast_login(Date last_login) {
        this.last_login = last_login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }
}
