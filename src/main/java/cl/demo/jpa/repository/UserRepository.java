package cl.demo.jpa.repository;

import cl.demo.jpa.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findFirstByEmail(String email);
}
